# Qiibee test app

This is an Expo app. Expo is a dev toolkit for developping React Native applications.

### Running Dev

1. Clone
2. Run `yarn`
5. Open the project with Expo XDE / the `exp` cli

### Running the App on demo mode

1. Install Expo on your iOS / Android phone
2. Scan the QR code using the Expo app: https://expo.io/@anaibol/qiibee-test-app

What I did on the Frontend:
- Expo project and Dev env setup (prettier / eslint)
- Apollo setup
- Folder structure with screens (for routing with React Navigation), containers and components
- Currencies Service
- Routes and components for Login
- Routes and Tab navigation for Wallets, Transactions and Settings (for logout / default currency)
- Base reusable components

This is a work in progress, so many things are need to be done:
- Transform stateless class components to functions
- Improve the UI / layout of the Wallet
- For authenticating the user before executing a transaction I would use the Expo integrated feature for `Fingerprint`. Checking if it supported by the phone with `Fingerprint.hasHardwareAsync` and `Fingerprint.isEnrolledAsync()` and authenticating with `Fingerprint.authenticateAsync()`
https://docs.expo.io/versions/latest/sdk/fingerprint

- Refreshing the the currencies values on realtime I would use a Websockets endpoint to update the rates.
- Testing I would use `jest` and `enzyme`. Since it's a financial app I would also like to try typescript to enforce typing.



