import React, { Component } from 'react'
import { graphql } from 'react-apollo/graphql'
import gql from 'graphql-tag'
import { Query } from 'react-apollo'

import Transactions from '../components/Transactions'

const GET_TRANSACTION = gql`
  query getTransactions {
    getTransactions(userId: $userId) {
      id
      from
      to
    }
  }
`

@graphql(GET_TRANSACTION)
export default class TransactionsScreen extends Component {
  render() {
    const { transactions } = this.props

    return <Transactions transactions={transactions} />
  }
}
