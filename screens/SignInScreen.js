import React, { Component } from 'react'

import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'

import SignIn from '../components/SignIn'

export default class SignInScreen extends Component {
  static navigationOptions = {
    header: null
  }

  render() {
    const { navigation } = this.props

    return (
      <Mutation
        mutation={gql`
          mutation signIn($email: String!, $password: String!) {
            signIn(email: $email, password: $password) {
              id
              token
            }
          }
        `}>
        {(signIn, { data, loading, error }) => (
          <SignIn
            loading={loading}
            error={error}
            navigation={navigation}
            onSignIn={
              credentials => navigation.navigate('Main')
              // signIn({ variables: credentials })
            }
          />
        )}
      </Mutation>
    )
  }
}
