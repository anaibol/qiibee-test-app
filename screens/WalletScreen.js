import React, { Component } from "react"
import { graphql } from "react-apollo/graphql"
import gql from "graphql-tag"
import { Mutation } from "react-apollo"

import Wallet from "../components/Wallet"

const GET_USER = gql`
  {
    me
  }
`

const CREATE_TRANSACTION = gql`
  mutation createTransaction($currency: String!, $amount: Float!) {
    createTransaction(currency: $currency, amount: $amount) {
      id
    }
  }
`

@graphql(GET_USER)
@graphql(CREATE_TRANSACTION, {
  name: "createTransaction"
})
export default class WalletScreen extends Component {
  render() {
    const { createTransaction, data } = this.props

    return <Wallet createTransaction={createTransaction} data={data} />
  }
}
