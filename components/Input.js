import React from 'react'
import { TextInput, StyleSheet } from 'react-native'

export default function({ style, underlineColorAndroid = 'transparent', ...props }) {
  return <TextInput {...props} style={[style, styles.input]} />
}

const styles = StyleSheet.create({
  input: {
    color: 'white',
    height: 50,
    borderRadius: 5,
    borderWidth: 2,
    fontSize: 16,
    fontFamily: 'Raleway'
  }
})
