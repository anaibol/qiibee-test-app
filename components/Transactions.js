import React, { Component } from 'react'
import { ActivityIndicator, FlatList } from 'react-native'

import { TextInput, Text, TouchableOpacity, Alert, AsyncStorage, View, StyleSheet } from 'react-native'
import { log } from 'async'

const defaultEmail = 'Anibal'
const defaultPassword = 'StrongPassword9182923!!D$'

export default class Wallet extends Component {
  render() {
    const { loading, error } = this.props

    // const { email, password } = this.state
    const data = currencies.map(c => c)

    return (
      <View style={styles.container}>
        <FlatList data={data} renderItem={({ item }) => <Text>{item.key}</Text>} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#29EEB8'
  },
  title: {
    fontFamily: 'Raleway',
    fontSize: 32,
    marginBottom: 50
  },
  input: {
    color: 'white',
    height: 50,
    backgroundColor: '#23DAA8',
    marginBottom: 30,
    paddingLeft: 50,
    paddingRight: 50,
    fontSize: 18,
    fontFamily: 'Raleway',
    width: 200,
    textAlign: 'center'
  },
  button: {
    borderWidth: 0.1,
    borderColor: 'white',
    padding: 15,
    borderRadius: 5,
    backgroundColor: '#FFD502',
    marginLeft: 15,
    marginRight: 15,
    marginTop: 20,
    width: 200
  },
  buttontText: {
    fontFamily: 'Raleway',
    color: 'white',
    fontSize: 18,
    textAlign: 'center'
  }
})
