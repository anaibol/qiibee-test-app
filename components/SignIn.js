import React, { Component } from 'react'
import { ActivityIndicator, ImageBackground, Image } from 'react-native'
import { Ionicons } from '@expo/vector-icons'

import { TouchableOpacity, Alert, AsyncStorage, View, StyleSheet } from 'react-native'

import Text from './Text'
import Input from './Input'

const defaultEmail = 'Anibal'
const defaultPassword = 'StrongPassword9182923!!D$'

const logo = require('../assets/images/logo.png')
const backgroundImage = require('../assets/images/background.png')

export default class SignIn extends Component {
  constructor() {
    super()

    this.state = {
      email: defaultEmail,
      password: defaultPassword
    }

    this.loadCredentials()
  }

  async loadCredentials() {
    try {
      const [email, password] = await Promise.all([AsyncStorage.getItem('email'), AsyncStorage.getItem('password')])

      this.setState({ email, password })
    } catch (e) {
      console.log(e)
    }
  }

  handleOnSignIn = () => {
    try {
      const { navigation, onSignIn } = this.props
      const { email, password } = this.state

      onSignIn({ email, password })
    } catch (e) {
      console.log(e)
    }
  }

  render() {
    const { loading, error } = this.props

    const { email, password } = this.state

    return (
      <ImageBackground source={backgroundImage} style={styles.container}>
        <Image source={logo} style={styles.logo} />
        <Text style={styles.title}>Signin</Text>
        <Input
          style={styles.input}
          onChangeText={email =>
            this.setState({
              email
            })
          }
          placeholder="Email"
          type="email"
          placeholderTextColor="white"
          returnKeyType="next"
          autoCapitalize="none"
          onSubmitEditing={() => this.passwordInput.focus()}
          autoCorrect={false}
          defaultValue={defaultEmail}
        />
        <Input
          style={styles.input}
          placeholder="Password"
          placeholderTextColor="white"
          returnKeyType="go"
          secureTextEntry
          defaultValue={defaultPassword}
        />
        <View>
          <TouchableOpacity style={styles.button} onPress={this.handleOnSignIn}>
            {!loading && <Text style={styles.buttonText}>Sign in</Text>}
            {loading && <ActivityIndicator />}
          </TouchableOpacity>
        </View>
      </ImageBackground>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#29EEB8'
  },
  logo: {},
  title: {
    fontSize: 32,
    marginVertical: 50,
    color: 'white'
  },
  input: {
    color: 'white',
    backgroundColor: 'rgba(0, 0, 0, .65)',
    borderColor: 'rgba(0, 0, 0, .75)',
    marginBottom: 30,
    paddingHorizontal: 50,
    fontSize: 18,
    width: 200,
    textAlign: 'center'
  },
  button: {
    borderWidth: 0.1,
    borderColor: 'white',
    padding: 15,
    borderRadius: 5,
    backgroundColor: '#3981CD',
    borderRadius: 5,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 20,
    width: 200
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center'
  }
})
