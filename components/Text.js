import React from 'react'
import { Text as RNText, StyleSheet } from 'react-native'

export default function Text({ style, ...props }) {
  return <RNText {...props} style={[style, styles.text]} />
}

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Raleway'
  }
})
