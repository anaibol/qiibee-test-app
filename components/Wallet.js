import React, { Component } from 'react'
import { ActivityIndicator, FlatList } from 'react-native'
import { TextInput, Text, View, StyleSheet } from 'react-native'
import { ListItem } from 'react-native-elements'

import currenciesService from '../utils/currenciesService'

const defaultEmail = 'Anibal'
const defaultPassword = 'StrongPassword9182923!!D$'

export default class Wallet extends Component {
  state = {
    currencies: []
  }

  constructor(props) {
    super(props)

    this.loadCurrencies()
  }

  async loadCurrencies() {
    try {
      const defaultCurrency = 'USD'

      const currencies = await currenciesService.get(defaultCurrency, ['USD', 'EUR', 'CHF'])

      console.log(currencies)

      this.setState({ currencies })
    } catch (e) {
      console.log(e)
    }
  }

  renderCurrency = ({ symbol, value }) => {
    return (
      <Text>
        {symbol}
        {value}
      </Text>
    )
  }

  render() {
    const { loading, error } = this.props
    const { currencies } = this.state

    return (
      <View style={styles.container}>
        <FlatList keyExtractor={item => item.symbol} data={currencies} renderItem={this.renderCurrency} />
      </View>
    )
  }
}

const styles = StyleSheet.create({})
