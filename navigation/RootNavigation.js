import React, { Component } from 'react'
import { StackNavigator } from 'react-navigation'

import MainTabNavigator from './MainTabNavigator'

import AuthLoadingScreen from '../screens/AuthLoadingScreen'
import SignInScreen from '../screens/SignInScreen'

const RootStackNavigator = StackNavigator(
  {
    Main: {
      screen: MainTabNavigator
    },
    AuthLoading: AuthLoadingScreen,
    SignIn: SignInScreen
  },
  {
    initialRouteName: 'AuthLoading',
    navigationOptions: () => ({
      headerTitleStyle: {
        fontWeight: 'normal',
        fontFamily: 'Raleway'
      }
    })
  }
)

export default class RootNavigator extends Component {
  render() {
    return <RootStackNavigator />
  }

  signOutAsync = async () => {
    const { navigation } = this.props

    await AsyncStorage.clear()

    navigation.navigate('SignIn')
  }
}
