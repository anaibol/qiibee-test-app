import React from 'react'
import { Platform } from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import { TabNavigator, TabBarBottom } from 'react-navigation'

import Colors from '../constants/Colors'

import WalletScreen from '../screens/WalletScreen'
import TransactionsScreen from '../screens/TransactionsScreen'

export default TabNavigator(
  {
    Wallet: {
      screen: WalletScreen
    },
    Transactions: {
      screen: TransactionsScreen
    },
    Settings: {
      screen: TransactionsScreen
    }
  },
  {
    navigationOptions: ({ navigation }) => ({
      header: null,
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state
        let iconName

        switch (routeName) {
          case 'Wallet':
            iconName = Platform.OS === 'ios' ? `ios-apps${focused ? '' : '-outline'}` : 'md-apps'
            break
          case 'Transactions':
            iconName = Platform.OS === 'ios' ? `ios-clock${focused ? '' : '-outline'}` : 'md-clock'
            break
          case 'Settings':
            iconName = Platform.OS === 'ios' ? `ios-options${focused ? '' : '-outline'}` : 'md-options'
        }
        return (
          <Ionicons
            name={iconName}
            size={28}
            style={{ marginBottom: -3, width: 25 }}
            color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
        )
      }
    }),
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false
  }
)
