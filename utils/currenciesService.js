import queryString from 'query-string'

const config = {
  endpoint: 'http://www.apilayer.net/api/live',
  accessKey: '45c7b48597f06d05e004ec311172bc66'
}

async function get(defaultCurrency, symbols) {
  const params = queryString.stringify({
    access_key: config.accessKey,
    source: defaultCurrency,
    currencies: symbols.join(','),
    format: 1
  })

  const res = await fetch(config.endpoint + '?' + params)

  if (res.ok) {
    const { quotes } = await res.json()

    const currencies = Object.keys(quotes).map(quoteSymbol => {
      const symbol = quoteSymbol.split(defaultCurrency)[1]

      return {
        symbol: defaultCurrency === symbol ? defaultCurrency : symbol,
        value: quotes[quoteSymbol]
      }
    })

    return currencies
  } else {
    throw new Error(res.status)
  }
}

export default {
  get
}
